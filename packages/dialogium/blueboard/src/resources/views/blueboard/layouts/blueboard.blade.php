<html>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style type="text/css" media="all">
body{
	background-color: #2a88bd;
        color:white;
}
.panel-body{
    color: #636b6f;
}

.blueboard a{
    color:white;
}

.jumbotron{
        background-color: #135;
        border-radius:2px;
}
.btn-success{
    background-color:#87cf25;
}

.btn-primary{
    background-color: #3a859a;
}

</style>
<body>
    <div id="app">
        <div class="container">
            @include ('inc.messages')
            @yield ('content')
        </div>
    </div>
</body>

</html>
