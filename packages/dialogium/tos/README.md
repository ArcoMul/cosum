# Terms of service

[![Build Status](https://travis-ci.org/dialogium/tos.svg?branch=master)](https://travis-ci.org/dialogium/tos)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/dialogium/tos/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/dialogium/tos/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/dialogium/tos/badge.svg?branch=master)](https://coveralls.io/github/dialogium/tos?branch=master)

[![Packagist](https://img.shields.io/packagist/v/dialogium/tos.svg)](https://packagist.org/packages/dialogium/tos)
[![Packagist](https://poser.pugx.org/dialogium/tos/d/total.svg)](https://packagist.org/packages/dialogium/tos)
[![Packagist](https://img.shields.io/packagist/l/dialogium/tos.svg)](https://packagist.org/packages/dialogium/tos)

Package description: CHANGE ME

## Installation

Install via composer
```bash
composer require dialogium/tos
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Dialogium\tos\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Dialogium\tos\Facades\tos::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Dialogium\tos\ServiceProvider" --tag="config"
```

## Usage

CHANGE ME

## Security

If you discover any security related issues, please email michael@dialogium.de
instead of using the issue tracker.

## Credits

- [Michael Scheppat](https://github.com/dialogium/tos)
- [All contributors](https://github.com/dialogium/tos/graphs/contributors)

This package is bootstrapped with the help of
[melihovv/laravel-package-generator](https://github.com/melihovv/laravel-package-generator).
