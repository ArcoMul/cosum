<?php

namespace Dialogium\tos\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Dialogium\tos\Controllers\Controller as cntr;
use Illuminate\Foundation\Auth\RedirectsUsers;
use TOSClassAlias;

class TosController extends cntr
{
    use RedirectsUsers;
    /*
    |--------------------------------------------------------------------------
    | Tos acception Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling presentation and agreement of Terms of Service (ToS) for any
    | user that recently registered with the application. 
    |
    */

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function latest(Request $request){
        //If no TOS passed pass user through
        if (TOSClassAlias::count() <= 0){
            return view('auth.notos');
        }
        $latest=TOSClassAlias::latest()->first();
        return view('auth.tos')->with('latest',$latest);
    }

    public function agree(Request $request, $id){
        $user=$request->user();
        $user->setToS($id);
        $user->save();
        return redirect($this->redirectPath())->with('message', 'Vielen Dank!');
    }

}
