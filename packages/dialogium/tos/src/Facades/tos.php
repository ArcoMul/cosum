<?php

namespace Dialogium\tos\Facades;

use Illuminate\Support\Facades\Facade;

class tos extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tos';
    }
}
