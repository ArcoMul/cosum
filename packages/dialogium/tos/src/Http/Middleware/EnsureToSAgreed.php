<?php

namespace Dialogium\tos\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use TOSClassAlias;
class EnsureToSAgreed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $user=$request->user();

        //If no TOS passed pass user through
        if (TOSClassAlias::count() <= 0){
            return $next($request);
        }

        if (! $user ||(! ($user->hasToS()))) { // also check ($user instanceof MustAgreeToS)
            return $request->expectsJson()
                    ? abort(403, 'Nutzungsbedingungen noch nicht zugestimmt.')
                    : redirect('tos/latest');
        }

        return $next($request);
    }
}
