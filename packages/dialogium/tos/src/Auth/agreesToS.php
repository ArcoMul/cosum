<?php

namespace Dialogium\tos\Auth;

Trait agreesToS{
    public function hasToS(){
        return !(is_null($this->tos_id));
    }

    public function setToS($id){
        $this->tos_id=$id;
        $this->save();
    }

    public function tos(){
        return TOSClassAlias::find($this->tos_id);
    }
}
