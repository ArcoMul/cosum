<?php

/*
 * This file is part of Laravel Ban.
 *
 * (c) Anton Komarev <a.komarev@cybercog.su>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Dialogium\OperatorMiddleware\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard as StatefulGuardContract;
use Dialogium\OperatorMiddleware\Contracts\Operatorable as OperatorContract;

/**
 * Class OperatorUser.
 *
 * @package Cog\Laravel\Ban\Http\Middleware
 */
class MyOperatorUser
{
    /**
     * The Guard implementation.
     *
     * @var \Illuminate\Contracts\Auth\Guard
     */
    protected $auth;

    /**
     * @param \Illuminate\Contracts\Auth\Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $user = $this->auth->user();

        if ($user && $user instanceof OperatorContract && $user->isOperator() ) {
            return $next($request);
        }

        $this->auth->logout();

        return redirect()->back()->withInput()->withErrors([
            'login' => __('Zugriff verweigert.'),
        ]);
    }
}
