<?php

namespace Dialogium\OperatorMiddleware\Traits;

//use Carbon\Carbon;

trait Operatorable
{
    //use HasOperatorableHelpers;
    /**
     * Set operator flag.
     *
     * @return $this
     */
    public function setOperatorFlag()
    {
        $this->operator = now();

        return $this;
    }

    /**
     * Unset operator flag.
     *
     * @return $this
     */
    public function unsetOperatorFlag()
    {
        $this->operator = null;

        return $this;
    }

    /**
     * If model is operator.
     *
     * @return bool
     */
    public function isOperator()
    {
        return $this->operator !== null;
    }

    /**
     * If model is not operator.
     *
     * @return bool
     */
    public function isNotOperator()
    {
        return !$this->isOperator();
    }
    //use HasBannedAtScope;
    //use HasBansRelation;
}
