<?php

namespace App\Traits;

use Carbon\Carbon;

/**
 * Trait HasOperatorAtHelpers.
 *
 * @package App\Op\Traits
 */
trait HasOperatorHelpers
{
    /**
     * Set operator flag.
     *
     * @return $this
     */
    public function setOperatorFlag()
    {
        $this->operator = Carbon::now();

        return $this;
    }

    /**
     * Unset operator flag.
     *
     * @return $this
     */
    public function unsetOperatorFlag()
    {
        $this->operator = null;

        return $this;
    }

    /**
     * If model is operator.
     *
     * @return bool
     */
    public function isOperator()
    {
        return $this->operator !== null;
    }

    /**
     * If model is not operator.
     *
     * @return bool
     */
    public function isNotOperator()
    {
        return !$this->isOperator();
    }

}
