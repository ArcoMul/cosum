<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;
use App\Notifications\VerifyEmail;
use Cog\Contracts\Ban\Bannable as BannableContract;
use Cog\Laravel\Ban\Traits\Bannable;
use Dialogium\OperatorMiddleware\Contracts\Operatorable as OperatorContract;
use Dialogium\OperatorMiddleware\Traits\Operatorable;
use Dialogium\tos\Contracts\Auth\MustAgreeToS;
use Dialogium\tos\Auth\agreesToS;
use App\Regkey;
use App\SimpleMsg;
use App\ExpiryReminder;
use App\ContactCircle;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail,MustAgreeToS,BannableContract,OperatorContract
{
    use Notifiable;
    use Bannable;
    use Operatorable;
    use agreesToS;

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'auto_connect','last_login_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function itemRequests(){
        return $this->hasMany('App\ItemRequest');
    }

    public function kontakte(){
        return $this->hasMany('App\Kontakt');
    }

    public function contacts(){
        return $this->hasMany('App\Contact', 'owner_id');
    }

    public function regkey(){
        return $this->belongsToMany('App\Regkey', 'keyhooks','user_id','regkey_id')->withTimestamps()->first();
    }

    public function regkeys(){
        return $this->belongsToMany('App\Regkey', 'keyhooks','user_id','regkey_id')->withTimestamps();
    }

    public function subjgruppen(){
        return $this->hasMany('App\SubjGruppe');
    }

    public function orte(){
        return $this->hasMany('App\Ort');
    }

    public function contactcircles(){
        return $this->hasMany('App\ContactCircle');
    }

    public function lendingprocesses(){
        return $this->hasMany('App\Lendingprocess');
    }

    public function gegenstaende(){
        return $this->hasMany('App\Gegenstand');
    }

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function sendEmailVerificationNotification(){
            $this->notify(new \App\Notifications\VerifyEmail());
    }

    public function sendLendingRequestNotification($url,$sender, $gegenstand_titel, $msg_body){
            $this->notify(new \App\Notifications\LendingRequestEmail($url,$sender,$gegenstand_titel, $msg_body));
    }

    public function sendLendingExpiryNotification($url,$sender, $msg_body, $expiryReminder){
            $this->notify(new \App\Notifications\LendingExpiryEmail($url,$sender,$msg_body, $expiryReminder));
    }

    public function sendLookingForRequestNotification($url,$sender, $gegenstand_titel, $msg_body){
            $this->notify(new \App\Notifications\LookingForRequestEmail($url,$sender,$gegenstand_titel, $msg_body));
    }

    public function sendPasswordResetNotification($token) {
            $this->notify(new \App\Notifications\MyResetPassword($token));
    }

    public function is_connected_to(User $user){
       return ($user && $this->contacts()->pluck('correspondent_id')->contains($user->id));
    }

    public function is_valid(){
        return $this->admitted;
    }

    public function validate(){
        return $this->admitted=true;
    }

    public function invalidate(){
        return $this->admitted=false;
    }

    public function auto_connect(User $user, Regkey $regkey){
        if (!$user) {
            return false;
        }

        //prevent connection to himself
        if ($this->is($user)){
            return true;
        }

        $c1=null;
        $c2=null;
        //already among contacts
        if ($this->is_connected_to($user)){
            $c1= $this->contacts()->where('correspondent_id', $user->id)->first();//my contact to him
            $c2= $user->contacts()->where('correspondent_id', $this->id)->first();//his contact to me
        }else{
            //create my contact to him
            $c1=new Contact;
            $c1->called = $user->name;
            $c1->owner_id = $this->id;
            $c1->save();

            //create his contact to me
            $c2=new Contact;
            $c2->called = $this->name;
            $c2->owner_id = $user->id;
            $c2->save();

            $c1->auto_connect($c2);
        }

        //handle community circle
        // check existence of community circle by name
        $communitycircle= $this->contactcircles()->where('name', $regkey->keyname )->first();

        if (!$communitycircle){
           $communitycircle= $this->create_community_circle( $regkey );
        }
        // add contact to that auto user to community circle
        if (!$communitycircle->contacts()->get()->contains('id',$c1->id))
            $communitycircle->contacts()->attach($c1);

        //update other contact/community circle
        $other= User::find($c2->owner_id);
        $other_communitycircle= $other->contactcircles()->where('name', $regkey->keyname )->first();
        if (!$other_communitycircle){
           $other_communitycircle= $other->create_community_circle( $regkey );
        }
        if (!$other_communitycircle->contacts()->get()->contains('id',$c2->id))
            $other_communitycircle->contacts()->attach($c2);

        return true;
    }

    function new_messages(){
        return SimpleMsg::where('to_user_id',$this->id)->where('read',0)->get()->count();
    }

    function verify_email_now(){
        $this->email_verified_at = now();
        $this->save();
    }

    function bells(){
        return $this->new_messages();
    }

    function create_community_circle( Regkey $regkey ){
            $contactcircle = new ContactCircle;
            $contactcircle->name = $regkey->keyname;
            $contactcircle->user_id = $this->id;
            $contactcircle->community=true;
            $contactcircle->save();

            return $contactcircle;
    }

    function email_notifications_on(){
       return $this->profile()->first()->email_notifications_on;
    }
}
