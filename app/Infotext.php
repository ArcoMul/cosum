<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infotext extends Model
{
    // Table Name
    protected $table = 'infotexts';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User');
    }
}
