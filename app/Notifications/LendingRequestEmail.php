<?php

namespace App\Notifications;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class LendingRequestEmail extends Notification
{

    public $url="";
    public $sender="jemand";
    public $msg_body="";
    public $gegenstand_titel="";

    public function __construct($url,$sender, $gegenstand_titel, $msg_body)
    {
        $this->url= $url;
        $this->msg_body= $msg_body;
        $this->gegenstand_titel= $gegenstand_titel;
        $this->sender= $sender;
    }
    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function url(){
        return $this->url;
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        return (new MailMessage)
            ->subject('Leih-Anfrage')
            ->greeting('Hallo!')
            ->line( $this->sender . ' hat Interesse an dem Gegenstand: ')
            ->line( new HtmlString ("<h2>". $this->gegenstand_titel ."</h2>" ))
            ->line( new HtmlString ( $this->msg_body ))
            ->action(
                'Hier zur Anfrage',
                $this->url()
            )
            ->line('Gar nicht bei COSUM registriert? Dann kann diese Email ignoriert werden.')
            ->salutation('Mit besten Grüßen, Cosum');
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
