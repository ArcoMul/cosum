<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispersion extends Model
{
    const ISPUBLIC = 'public';
    const COMMUNAL = 'communal';
    const ISPRIVATE = 'private';
    const HIDDEN = 'hidden';
}
