<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ExpiryReminder;
use Carbon\Carbon;

class ProvokeReminderMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'provoke:ReminderMails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provokes ReminderMails. Fires only if reminder condition met.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("[". Carbon::now() ."] ". "Provoking EmailReminders...");
        $emailReminders=ExpiryReminder::all();

        forEach ($emailReminders as $er)
            $er->fire(); // send notfication mail of that reminder iff
        $this->info("EmailReminders provoked.");
    }
}
