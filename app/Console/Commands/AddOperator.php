<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\User;

class AddOperator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'operator:new
                        {username : A short nickname}
                        {email : The email address for the account}
                        {verified : Wether email is considered to be verfied}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will add a new operator to the system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check whether a password is too weak.
     *
     * @return 
     * */ 
   /* public function nopass($pw){
        return strlen($pw) < 8;
    } */

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $prompt="Please enter pw";
        $pass=false;
        $password="";
        while (!$pass){
            $password=$this->secret($prompt);
            $len=strlen($password);
            if (/*nopass($password)*/$len < 8){
                echo "Password too weak. Try again. 8 digits min. Yours had:$len \n";
            }else{
                $pass=true;
                $this->info ("Password ok!");
                $username=$this->argument('username');
                $email=$this->argument('email');
                $verified=$this->argument('verified');
                
                $newuser=\App\User::create(['name' =>$username,'email' => $email,'password' => bcrypt($password) ]);
                $newuser->operator= now();
                $newuser->admitted=1;
                if ($verified == 'verified' )
                    $newuser->email_verified_at= now();
                $newuser->save();
                $this->info("User $newuser->name created!");
            }
        }
    }

}
