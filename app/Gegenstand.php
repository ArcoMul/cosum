<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Gegenstand extends Model
{
    // Table Name
    protected $table = 'gegenstands';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    protected $appends = [
        'over_due'
    ];

    public static $categories=["Baby & Kind", "Haushalt", "Garten", "Werkzeug", "Sport & Spiele", "Outdoor", "Medien", "Elektronik", "Sonstiges", "alle" ];

    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function toggle_block(){
        $this->blocked= !$this->blocked;
        $this->save();
    }

    public function getOverDueAttribute(){
        return $this->isOverDue();
    }

    public function isOverDue(){
        if ($this->lent_to_date == null)
            return false;

        //$exampleDate = Carbon::parse('2023-07-25 13:10:12');
        $firstDate = Carbon::today();
        $secondDate = Carbon::parse($this->lent_to_date);

        return $firstDate->gt($secondDate);
    }
}
