<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Proposal;
use DB;

class ProposalsController extends Controller
{
    protected $view_source='';//blueboard::';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'operator-user']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proposals = Proposal::orderBy('created_at','desc')->paginate(10);
        return view($this->view_source.'proposals.index')->with('proposals', $proposals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view_source.'proposals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        // Create Proposal
        $proposal = new Proposal;
        $proposal->title = $request->input('title');
        $proposal->body = $request->input('body');
        $proposal->user_id = auth()->user()->id;
        $proposal->save();

        return redirect('/proposals')->with('success', 'Vereinbarungstext hinzugefügt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proposal = Proposal::find($id);
        return view($this->view_source.'proposals.show')->with('proposal', $proposal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proposal = Proposal::find($id);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Seite');
        }

        return view($this->view_source.'proposals.edit')->with('proposal', $proposal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }
        
        // Create Proposal
        $proposal = Proposal::find($id);
        $proposal->title = $request->input('title');
        $proposal->body = $request->input('body');
        $proposal->user_id= auth()->user()->id;
        $proposal->save();

        return redirect('/proposals')->with('success', 'Proposal Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proposal = Proposal::find($id);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }
        
        $proposal->delete();
        return redirect('/proposals')->with('success', 'Proposal Removed');
    }
}
