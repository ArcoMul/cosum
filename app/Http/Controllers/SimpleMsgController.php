<?php

namespace App\Http\Controllers;

use App\Gegenstand;
use App\User;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\SimpleMsg;

class SimpleMsgController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }

    public function index(){
        $smsgs= SimpleMsg::where('to_user_id', auth()->user()->id)->orderBy('created_at','desc')->paginate(10);
        $sent_smsgs= SimpleMsg::where('from_user_id', auth()->user()->id)->orderBy('created_at','desc')->paginate(10);
        return view('simple_msgs.index')->with(['smsgs'=>$smsgs, 'sent_smsgs' => $sent_smsgs]);
    }

    public function create($gid, Request $request){
        $gegenstand = Gegenstand::find($gid);
        if (!$gegenstand){
            return redirect()->back()->with('error', 'Gegenstand nicht gefunden.' );
        }

        return view('simple_msgs.create')->with(['gegenstand' => $gegenstand, 'sender_name' => auth()->user()->name ]);
    }

    public function show($id){
        $smsg=SimpleMsg::find($id);
        $gegenstand=Gegenstand::find($smsg->item_id);

        if (!$gegenstand){
            $smsg->delete();
            return back()->with('error','Gegenstand existiert nicht mehr. Anfrage gelöscht.');
        }

        $view_mode=($smsg->signal == null)?'show':'read';
        //recipient
        if ($smsg->to_user_id == auth()->user()->id && $gegenstand && $gegenstand->user_id == auth()->user()->id ){
            $smsg->read=true;
            $smsg->save();

            //format dates
            $ldatef=new \DateTime($smsg->desired_date_from);
            $smsg->desired_date_from=$ldatef->format('d.m.Y');
            $ldatet=new \DateTime($smsg->desired_date_to);
            $smsg->desired_date_to=$ldatet->format('d.m.Y');

            return view('simple_msgs.'.$view_mode)->with(['msg'=> $smsg, 'gegenstand' => $gegenstand]);
        }
            //format dates
            $ldatef=new \DateTime($smsg->desired_date_from);
            $smsg->desired_date_from=$ldatef->format('d.m.Y');
            $ldatet=new \DateTime($smsg->desired_date_to);
            $smsg->desired_date_to=$ldatet->format('d.m.Y');

        //sender
        if ($smsg->from_user_id == auth()->user()->id && $gegenstand)
            return view('simple_msgs.read')->with(['msg'=> $smsg, 'gegenstand' => $gegenstand]);

        return redirect()->back()->with('error', 'Kein Zugriff.');
    }

    public function reply($id, Request $request){
        $this->validate($request, [
            'item_id' => 'required',
            'from_user_id' => 'required',
            'to_user_id' => 'required'
        ]);

        $msg=SimpleMsg::find($id);
        $item_id=$msg->item_id;;

        if ($msg->to_user_id !== auth()->user()->id )
            return redirect('/gegenstaende/'.$item_id)->with('error','Kein Zugriff.');

        $msg->reply= $request->input('reply'); 
        $msg->signal= $request->input('signal'); 
        $msg->save();
        return redirect('/gegenstaende/'.$item_id)->with('success','Anfrage beantwortet.');
    }

    public function store(Request $request){
        $this->validate($request, [
            'item_id' => 'required',
            'from_user_id' => 'required',
            'to_user_id' => 'required'
        ]);
        $item_id=$request->input('item_id');
        $to_user_id=$request->input('to_user_id'); 
        $gegenstand=Gegenstand::find($item_id);

        if ($gegenstand->user_id != $to_user_id){
            return redirect('/gegenstaende/'.$item_id)->with('error','Anfrage nicht möglich.');
        }

        $msg= new SimpleMsg;
        $msg->from_user_id = auth()->user()->id;
        $msg->to_user_id = $to_user_id; 
        $msg->item_id= $item_id;
        $msg->sender_name= $request->input('sender_name');
        $msg->text= $request->input('text'); 

        $raw_input_from=$request->input('desired_date_from');
        $raw_input_to=$request->input('desired_date_to');

        $from_timestamp = null;
        $to_timestamp = null;
        if ($raw_input_from)
            $from_timestamp = \DateTime::createFromFormat("d.m.Y", $raw_input_from)->format("Y-m-d")." 00:00:00";
        if ($raw_input_to)
            $to_timestamp = \DateTime::createFromFormat("d.m.Y", $raw_input_to)->format("Y-m-d")." 00:00:00";

        $msg->desired_date_from=$from_timestamp;
        $msg->desired_date_to=$to_timestamp;

        $msg->save();

        $recipient=User::with('profile')->find($msg->to_user_id);
        $additional_note="";
        $profile=$recipient->profile;
        if ($profile->email_notifications_on){
            $recipient->sendLendingRequestNotification(url('/anfrage')."/".$msg->id,$msg->sender_name, $gegenstand->name, $msg->text);
            $additional_note="Der Empfänger wird per Email informiert.";
        }

        return redirect('/gegenstaende/'.$item_id)->with('success','Anfrage entgegengenommen. '.$additional_note );
    }

    public function destroy($id){
        $smsg=SimpleMsg::find($id);
        if (!$smsg){
            return back()->with('error','Nachricht existiert nicht.');
        }

        if ($smsg->from_user_id == auth()->user()->id){
            $smsg->delete();
            return back()->with('success','Nachricht gelöscht.');
        }else
            return back()->with('error','Einbruch: Vorfall wird gemeldet.');
    }
}
