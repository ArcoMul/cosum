<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Gegenstand;
use DB;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos'], ['except' => ['pub_go']]);
    }

    public function go(Request $request){
        $gegenstaende=[];
        $myquery="";
        $myitemsonly=$request->myitemsonly;
        if ( ($request->search) != "")
        {
            if ($myitemsonly){
                $myquery= Gegenstand::where('user_id', auth()->user()->id );
            }else{
                $myquery= Gegenstand::where('dispersion','<>','private');
            }
                //search content
                $myquery->where(function ($query) use ($request){
                        $query->where('name', 'LIKE', "%$request->search%")
                              ->orWhere('beschreibung', 'LIKE', "%$request->search%")
                              ->orWhere('category', 'LIKE', "%$request->search%");
                    })->orderBy('created_at','desc');

        }else
            $myquery= Gegenstand::where('dispersion','<>','private')
                                ->orderBy('created_at','desc');//->get();//->paginate(10);

        $gegenstaende=$myquery->get();
        return view('search.go')->with(['search' => $request->search, 'gegenstaende' => $gegenstaende, 'myitemsonly' => $myitemsonly]);
    }

    public function pub_go(Request $request){
        $gegenstaende=[];
        if ( ($request->search) != "")
        {
            $gegenstaende= Gegenstand::where('dispersion','public')
                ->where(function ($query) use ($request){
                    $query->where('name', 'LIKE', "%$request->search%")
                          ->orWhere('beschreibung', 'LIKE', "%$request->search%")
                          ->orWhere('category', 'LIKE', "%$request->search%");
                })->orderBy('created_at','desc')->get();//paginate(10);
        }else
            $gegenstaende= Gegenstand::where('dispersion','public')
                                ->orderBy('created_at','desc')->get();//->paginate(10);

        return view('search.go')->with(['search' => $request->search, 'gegenstaende' => $gegenstaende]);
    }
}
