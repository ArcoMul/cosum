<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Token;
use App\Contact;
use App\Revelation;
use DB;

class ContactsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user=auth()->user();
	$contacts = Contact::where('owner_id', $user->id)->orderBy('called','asc')->get();
        foreach($contacts as $contact){
            $contact->status=$contact->status();
        }
        $view= ($request->lend)? 'contacts.lend_to' : 'contacts.index';
	return view($view)->with(['lend' => $request->lend,'obj_id' => $request->obj_id , 'corr_token'=> $request->corr_token ,'contacts' => $contacts,'contactcircles' => $user->contactcircles ] );
    }

    /**
     * Display a list to select resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request){
	    $contacts= \App\Contact::where('owner_id', auth()->user()->id)->orderBy('called','asc')->get();
            foreach($contacts as $contact){
                $contact->status=$contact->status();
            }
            return view( 'contacts.select' )->with([ 'contacts' => $contacts, 'returnAddress'  => $request->returnAddress ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
	return view('contacts.create')->with('corr_token', $request->corr_token );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	$this->validate($request, [
            'called' => 'required'
        ]);

	$contact= new Contact();
	$contact->called = $request->called;
	$contact->owner_id = auth()->user()->id;

        if ($request->contact_token){
                $correspondent= Contact::where('token', $request->contact_token )->first();
                if ($correspondent){
		    $contact->correspondent_id= $correspondent->owner_id;
		    $contact->correspondent_token= $request->contact_token;
                    if(auth()->user()->id == $contact->correspondent_id){
                        return redirect("/kontakte/{$id}/edit")->with('error', 'Kontakt zu sich selbst muss man anders aufbauen!');
                    }
                }else
		    return redirect("/kontakte/{$id}/edit")->with('error', 'Kontaktmarke gehört keinem Cosumer!');
	}

	$contact->save();
	return redirect('/kontakte/')->with('success', 'Kontakt angelegt.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	$contact= Contact::find($id);
        if (!$contact){
            return back()->with('error', 'Kontakt nicht gefunden.');
        }

        // Check for correct user
        if(auth()->user()->id !=$contact->owner_id){
            return redirect('/kontakte')->with('error', 'Zugriff verweigert.');
        }

	return view('contacts/show')->with('contact',$contact);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
	$contact= Contact::find($id);

        if (!$contact){
            return back()->with('error', 'Kontakt nicht gefunden.');
        }

        // Check for correct user
        if(auth()->user()->id !=$contact->owner_id){
            return redirect('/kontakte')->with('error', 'Zugriff verweigert.');
        }

        $status=$contact->status();
            //neuer Token
            if ($request->token == 'new' ){
                $new_token = Token::Unique('contacts', 'token', 12);
                $contact->new_token= strtolower($new_token);
            }

            //saved corresponding contact token. he is waiting to confirm.
            if ($contact->correspondent_token && $contact->token){
                $initiator=Contact::where('token',$contact->correspondent_token)->first();
                if($initiator){
                    //OK, you may create a link
                    $ci_id=$initiator->id;

                    $contact->initiator_url= "/kontakte/".$ci_id."/edit?corr_token=".$contact->token;
                }
            }

            //corresponding token just received and not yet saved.
            $corr_token="";
            if ($request->corr_token){
                $contact->initiator=Contact::where('token',$request->corr_token)->first();
                if(!$contact->initiator)
                    return redirect('/kontakte')->with('error', 'Kontaktpartner unbekannt.');

                //OK, you may save it
                $corr_token= $request->corr_token;
            }

	return view('contacts/edit')->with(['corr_token' => $corr_token, 'status' => $status , 'contact' => $contact]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	$this->validate($request, [
            'called' => 'required'
	]);

	$contact = Contact::find($id);
        // Check for correct user
        if(auth()->user()->id !=$contact->owner_id){
            return redirect('/kontakte')->with('error', 'Zugriff verweigert.');
        }

	$contact->called = $request->input('called');

	if ($request->contact_token){
                $correspondent= Contact::where('token', $request->contact_token )->first();
                if ($correspondent){
		    $contact->correspondent_id= $correspondent->owner_id;
		    $contact->correspondent_token= $request->contact_token;
                    if(auth()->user()->id == $contact->correspondent_id){
                        return redirect("/kontakte/{$id}/edit")->with('error', 'Kontakt zu sich selbst muss man anders aufbauen!');
                    }
                }else
		    return redirect("/kontakte/{$id}/edit")->with('error', 'Kontaktmarke gehört keinem Cosumer!');
	}

	if ($request->has('new_token')){
		$contact->token= $request->new_token;
	}

	$contact->save();

	return redirect("/kontakte/{$id}/edit")->with('success', 'Aktualisiert.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact_called= $contact->called;

        // Check for correct user
        if(auth()->user()->id !=$contact->owner_id){
            return redirect('/kontakte')->with('error', 'Löschen nicht erlaubt.');
        }

        $revelations_assoc= Revelation::where('rcpt_id',$id)->get();
        foreach($revelations_assoc as $rev){
            $rev->delete();
        }

        $contact->delete();
        return redirect('/kontakte')->with('success', 'Kontakt zu '.$contact_called.' gelöscht');
    }
}
