<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Carbon;
use Token;
use App\Gegenstand;
use App\Lendingprocess;
use App\Revelation;
use App\Contact;
use App\Ort;
use App\Dispersion;
use DB;
use URL;
use App\ExpiryReminder;
use App\User;

class GegenstaendeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth','verified','tos'], ['except' => ['index']]);
        $this->middleware(['logs-out-banned-user','auth','verified','tos'], ['except' => ['index', 'api_index','show']]);
    }

    function addOrtFilter($coll){
         //add hashed ort Filter
            $o_hash=Input::get('o');
            $h_ort=Ort::where('hash',$o_hash)->first();
            $ort_name="";
            if ($h_ort){
                $coll = $coll->where('ort_id',$h_ort->id);
                $ort_name=$h_ort->name;
            }

           return ["res" => $coll, 'error' => ($o_hash && !$h_ort), 'success' => ($o_hash && $h_ort) , 'name' => 'Ort', 'value' => $ort_name , 'ort' => $h_ort ];
    }

    function addItemFilter($coll){
         //add hashed Item Filter
            if (Input::get('g')){
                $g_hash=Input::get('g');
                $coll=$coll->where('hash',$g_hash);//->first();
            //if ($h_g)
            //    $coll = $coll->where('id',$h_g->id);
            }
           return ["res" => $coll, 'error' => false , 'name' => 'Gegenstand' ];
    }

    function isCategory($cat){
        $categories=Gegenstand::$categories;

        return in_array($cat,$categories);
    }

    function addCatFilter($coll,$cat){
        if  ($this->isCategory($cat)){
            return $coll->where('category',$cat);
        }
        return $coll;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $gegenstaende =[];
        //also visitors
        $gegenstaende = Gegenstand::where('dispersion','public');

        if ($request->has("cat")){
            $cat=$request->cat;
            $gegenstaende=$this->addCatFilter($gegenstaende, $cat);
        }

        //add hashed ort Filter
        $addedOrtFilter=$this->addOrtFilter($gegenstaende);
        if ($addedOrtFilter['error'])
                return redirect('/gegenstaende')->with('error', 'Ungültiger Ortsfilter.');
        $gegenstaende=$addedOrtFilter['res'];

        $filters = [];
        if ($addedOrtFilter['success'])
            array_push($filters,$addedOrtFilter );

        //add hashed item Filter
        $addedItemFilter=$this->addItemFilter($gegenstaende);
        //if ($addedItemFilter['error'])
            //return redirect('/gegenstaende')->with('error', 'Ungültiger Zugriffsscode.');
        $gegenstaende=$addedItemFilter['res'];

        if ($request->has("lent")){
            $gegenstaende = $gegenstaende->where('lent',true);
        }
        if ($request->has("due")){
            $gegenstaende = $gegenstaende->where('lent_to_date','<=',Carbon::today());
        }
        if ($request->has("time"))
            $gegenstaende= $gegenstaende->orderBy('created_at','desc');
        else /*default*/
            $gegenstaende= $gegenstaende->orderBy('name','asc');
        $gegenstaende = $gegenstaende->paginate(10);

	foreach($gegenstaende as $gegenstand){
                $ort= Ort::find($gegenstand->ort_id);
		$gegenstand->ort_name = $ort->name;
		if ($gegenstand->lent_to_date){
		    $ldate=new \DateTime($gegenstand->lent_to_date);
		    $gegenstand->lent_to_date= $ldate->format('d.m.Y');
                }
        }

        return view('gegenstaende.index')->with(['gegenstaende' =>  $gegenstaende, 'dispersion' => 'public', 'filters' => $filters ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function api_index()
    {
        $gegenstaende =[];
        //also visitors
        $gegenstaende = Gegenstand::where('dispersion','public');

        //add hashed ort Filter
        $addedOrtFilter=$this->addOrtFilter($gegenstaende);
        if ($addedOrtFilter['error'])
                return redirect('/gegenstaende')->with('error', 'Ungültiger Ortsfilter.');
        $gegenstaende=$addedOrtFilter['res'];

        //add hashed ort Filter
        $addedItemFilter=$this->addItemFilter($gegenstaende);
        //if ($addedItemFilter['error'])
            //return redirect('/gegenstaende')->with('error', 'Ungültiger Zugriffsscode.');
        $gegenstaende=$addedItemFilter['res'];

        $gegenstaende = $gegenstaende->orderBy('created_at','desc')->get();
	foreach($gegenstaende as $gegenstand){
                $ort= Ort::find($gegenstand->ort_id);
		$gegenstand->ort_name = $ort->name;
                $gegenstand->ort_plz= $ort->plz;
                $gegenstand->img_url = URL::to('storage/cover_images/'.$gegenstand->cover_image);
		if ($gegenstand->lent_to_date){
		    $ldate=new \DateTime($gegenstand->lent_to_date);
		    $gegenstand->lent_to_date= $ldate->format('d.m.Y');
		}
	}
        return $gegenstaende;
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index_member(Request $request)
    {
        $gegenstaende =[];
        $gegenstaende = Gegenstand::where('dispersion',Dispersion::COMMUNAL);

        if ($request->has("cat")){
            $cat=$request->cat;
            $gegenstaende=$this->addCatFilter($gegenstaende, $cat);
        }

        //add hashed ort Filter
        $addedOrtFilter=$this->addOrtFilter($gegenstaende);
        if ($addedOrtFilter['error'])
                return redirect('/kommunale_gegenstaende')->with('error', 'Ungültiger Ortsfilter.');
        $gegenstaende=$addedOrtFilter['res'];

        $filters = [];
        if ($addedOrtFilter['success'])
            array_push($filters,$addedOrtFilter );

        //add hashed item Filter
        $addedItemFilter=$this->addItemFilter($gegenstaende);
        //if ($addedItemFilter['error'])
            //return redirect('/gegenstaende')->with('error', 'Ungültiger Zugriffsscode.');
        $gegenstaende=$addedItemFilter['res'];

        if ($request->has("lent")){
            $gegenstaende = $gegenstaende->where('lent',true);
        }
        if ($request->has("due")){
            $gegenstaende = $gegenstaende->where('lent_to_date','<=',Carbon::today());
        }
        if ($request->has("time"))
            $gegenstaende= $gegenstaende->orderBy('created_at','desc');
        else /*default*/
            $gegenstaende= $gegenstaende->orderBy('name','asc');
        $gegenstaende = $gegenstaende->paginate(10);

	foreach($gegenstaende as $gegenstand){
		$gegenstand->ort_name = Ort::find($gegenstand->ort_id)->name;
		if ($gegenstand->lent_to_date){
		    $ldate=new \DateTime($gegenstand->lent_to_date);
		    $gegenstand->lent_to_date= $ldate->format('d.m.Y');
		}
	}
        return view('gegenstaende.index')->with(['gegenstaende' =>  $gegenstaende, 'dispersion' => 'communal', 'filters' => $filters]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function my_index(Request $request)
    {
        $user_id = auth()->user()->id;

        $gegenstaende = Gegenstand::where('user_id',$user_id);

        if ($request->has("cat")){
            $cat=$request->cat;
            $gegenstaende=$this->addCatFilter($gegenstaende, $cat);
        }

        if ($request->has("lent")){
            $gegenstaende = $gegenstaende->where('lent',true);
        }
        if ($request->has("due")){
            $gegenstaende = $gegenstaende->where('lent_to_date','<=',Carbon::today());
        }
        if ($request->has("time"))
            $gegenstaende= $gegenstaende->orderBy('created_at','desc');
        else /*default*/
            $gegenstaende= $gegenstaende->orderBy('name','asc');

        $gegenstaende = $gegenstaende->paginate(10);

	foreach($gegenstaende as $gegenstand){
		$gegenstand->ort_name = Ort::find($gegenstand->ort_id)->name;
		if ($gegenstand->lent_to_date){
		    $ldate=new \DateTime($gegenstand->lent_to_date);
		    $gegenstand->lent_to_date= $ldate->format('d.m.Y');
                }
                $gegenstand->lent_to_contact_id=null;
                $gegenstand->lent_to_called="";
                if ($gegenstand->lent_to){
                    $lent_to_contact_id=$gegenstand->lent_to;
                    $contact=Contact::find($lent_to_contact_id);
                    $gegenstand->lent_to_called="?";
                    if ($contact){
                        $contact_is_called=$contact->called;
                        $gegenstand->lent_to_called=$contact_is_called;
                        $gegenstand->lent_to_contact_id=$lent_to_contact_id;
                    }
                }

        }
        return view('gegenstaende.my_index')->with(['gegenstaende' =>  $gegenstaende, 'dispersion' => 'public' ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	$orte = Ort::where('user_id',auth()->user()->id)->get();
	if ($orte->count() <= 0 )
		return redirect('/orte/create?required_by=gegenstaende')->with('info', 'Bitte lege erst einen Ort für Gegenstände an.');
	$ortnamen= array_column($orte->toArray(),'name');
	$ortids= array_column($orte->toArray(),'id');
        return view('gegenstaende.create')->with(['ortnamen' => $ortnamen, 'ortids' => $ortids]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'beschreibung' => 'required',
            'ortid' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Handle File Upload
        if($request->hasFile('cover_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Gegenstand
        $gegenstand = new Gegenstand;
        $gegenstand->name = $request->input('name');
        $gegenstand->beschreibung = $request->input('beschreibung');
        $gegenstand->user_id = auth()->user()->id;
        $gegenstand->ort_id = $request->ortid;
        $gegenstand->cover_image = $fileNameToStore;
        $gegenstand->hash= Token::Unique('gegenstands', 'hash', 12);
        $gegenstand->save();

        //jump into item edit
        return redirect('/gegenstaende/'.$gegenstand->id.'/edit')->with('success', 'Gegenstand angelegt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gegenstand = Gegenstand::find($id);

        if (!$gegenstand)
            return abort(404);

	$gegenstand->ort_name = Ort::find($gegenstand->ort_id)->name;
        if ($gegenstand->lent_to_date){
	    $ldate=new \DateTime($gegenstand->lent_to_date);
	    $gegenstand->lent_to_date= $ldate->format('d.m.Y');
	}

        $ort = Ort::find($gegenstand->ort_id);

        $public = true; //default

        if ($gegenstand->dispersion == 'public')
            return view('gegenstaende.show')->with(['gegenstand' => $gegenstand , 'ort' => $ort]);

        if ($gegenstand->dispersion != 'public' && auth()->user())
            return view('gegenstaende.show')->with(['gegenstand' => $gegenstand , 'ort' => $ort]);

        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $gegenstand = Gegenstand::find($id);

        if(!$gegenstand){
            return redirect('/gegenstaende')->with('error', 'Nicht gefunden.');
        }
        // Check for correct user
        if(auth()->user()->id !=$gegenstand->user_id){
            return redirect('/gegenstaende')->with('error', 'Unauthorized Page');
        }

	$orte = Ort::all()->where('user_id','=',auth()->user()->id);
	$ortnamen= array_column($orte->toArray(),'name');
	$ortids= array_column($orte->toArray(),'id');
	$ortname = Ort::find($gegenstand->ort_id)->name;
        $lent_to_contact_id= null;
        if ($request->lent_to){
            $lent_to_contact_id=$request->lent_to;
        }elseif ($gegenstand->lent_to){
            $lent_to_contact_id=$gegenstand->lent_to;
            $gegenstand->lent_to_called="?";
        }

        $contact_is_called="";
        $contact=Contact::find($lent_to_contact_id);
        if ($contact){
            $contact_is_called=$contact->called;
            $gegenstand->lent_to_called=$contact_is_called;
            $gegenstand->lent_to_contact_id=$lent_to_contact_id;
        }

        if ($gegenstand->lent_to_date){
            $ldate=new \DateTime($gegenstand->lent_to_date);
            $gegenstand->lent_to_date= $ldate->format('d.m.Y');
        }

        return view('gegenstaende.edit')->with(['gegenstand' => $gegenstand , 'ortnamen' => $ortnamen, 'ortids' => $ortids, 'ortname' => $ortname, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'beschreibung' => 'required'
        ]);

         // Handle File Upload
        if($request->hasFile('cover_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        }

        // Find Gegenstand
        $gegenstand = Gegenstand::find($id);
        $lending_to_changed= false;
        $lending_to_date_changed= false;

        if(!$gegenstand){
            return redirect("/gegenstaende")->with('error', 'Ungültiger Gegenstand.');
        }
	$gegenstand->name = $request->input('name');
        $isgift= $request->input('isgift');
        $gegenstand->gift= ($isgift)? 1 : 0 ;
        //switch to lend the item
        if (!$gegenstand->lent && $request->input('islent') ){
            $lp=new Lendingprocess;
            $lp->user_id = Auth()->user()->id;
            $lp->gegenstand_id = $gegenstand->id;
            $lp->started_at = Carbon::now();
            $lp->save();
            $gegenstand->lendingprocess_id=$lp->id;
        }
        //switch to unlend the item
        if ($gegenstand->lent && !$request->input('islent') ){
            $lp_id=$gegenstand->lendingprocess_id;
            $lp=Lendingprocess::find($lp_id);
            if ($lp){
                $lp->finished_at=Carbon::now();
                $lp->save();
            }
            $gegenstand->lendingprocess_id=null;
        }
        $gegenstand->lent= $request->input('islent');
        if ($gegenstand->lent_to != $request->input('lent_to_contact_id'))
            $lending_to_changed= true;
	$gegenstand->lent_to= $request->input('lent_to_contact_id');
        $indate=$request->input('lent_to_date');
        if ($indate){ //save date
            $pdate=date_parse_from_format("j.n.Y", $indate);
            $outdate=$pdate['year']."-".$pdate['month']."-".$pdate['day'];
            if ($outdate != $gegenstand->lent_to_date){
                $lending_to_date_changed= true;
                $gegenstand->lent_to_date=$outdate;
            }
        }else //save no date
            $gegenstand->lent_to_date=null;

	$gegenstand->ort_id = $request->input('ortid');
	$gegenstand->dispersion= $request->input('dispersion');
	$gegenstand->category = $request->input('sel_category');
        $gegenstand->beschreibung = $request->input('beschreibung');
        if($request->hasFile('cover_image')){
            $gegenstand->cover_image = $fileNameToStore;
        }

        $lending_terms_changed = ($lending_to_changed || $lending_to_date_changed );

        if ($gegenstand -> lent && $gegenstand->lent_to && $gegenstand->lent_to_date && $lending_terms_changed){
            // createOneTimeReminder ( sender_contact, recepient_contact , date, item  )
            $lent_to_contact= Contact::find($gegenstand->lent_to);
            $c_user= User::find($lent_to_contact->correspondent_id);
            $filling=[ 'sender_contact_id' => ($lent_to_contact->corrContact()->id)??null , 'recepient_contact_id' => ($lent_to_contact->id)??null , 'expiry_date' => $gegenstand->lent_to_date, 'item_id' => $gegenstand->id ];
            $expiryReminder= ExpiryReminder::create($filling);
            $expiryReminder->save();
            //$c_user->sendLendingExpiryNotification(("/gegenstaende/".$gegenstand->id), null, "", $expiryReminder);
        }

        $gegenstand->save();

        return redirect("/gegenstaende/".$gegenstand->id."/edit")->with('success', 'Gegenstand aktualisiert');
    }

    /**
     *  Form for new revelation on this item
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reveal(Request $request, $id)
    {
        $gegenstand = Gegenstand::find($id);
        $contacts= auth()->user()->contacts()->orderBy('called','asc')->get();
        $made_contacts=[];
            foreach($contacts as $contact){
                $contact->status=$contact->status();
                if ($contact->status == 'made'){
                    array_push($made_contacts, $contact);
                }
            }

        $contactcircles = auth()->user()->contactcircles()->get();
        //render view create
        return view('gegenstaende.reveal')->with(['gegenstand' => $gegenstand, 'contacts' => $made_contacts, 'contactcircles' => $contactcircles, 'returnAddress' => '/gegenstaende/'.$id.'/edit' ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gegenstand = Gegenstand::find($id);

        // Check for correct user
        if(auth()->user()->id !=$gegenstand->user_id){
            return redirect('/meine_gegenstaende')->with('error', 'Unauthorized Page');
        }

        if($gegenstand->cover_image != 'noimage.jpg'){
            // Delete Image
            Storage::delete('public/cover_images/'.$gegenstand->cover_image);
        }

        $revelations_assoc= Revelation::where('obj_id',$id)->get();
        foreach($revelations_assoc as $rev){
            $rev->forceDelete();
        }
        $rks=auth()->user()->regkeys()->get();
        foreach ($rks as $community){
            $community->community_items()->detach($gegenstand->id);
        }

        //$gegenstand->delete(); //Gegenstand.softDeletes enabled and used as blocking feature
        $gegenstand->forceDelete(); //so really delete it here
        return redirect('/meine_gegenstaende')->with('success', 'Gegenstand gelöscht');
    }

    /**
     * Block the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggle_blocked($id)
    {
        $gegenstand = Gegenstand::find($id);

        if(!auth()->user()->isOperator()){
            return redirect('/meine_gegenstaende')->with('error', 'Unauthorisierte Aktion.');
        }

        // Check for correct user
        if(!$gegenstand){
            return redirect('/meine_gegenstaende')->with('error', 'Ungültiger Gegenstand.');
        }

        $gegenstand->toggle_block();

        $blocked_str=($gegenstand->blocked)? 'gesperrt.' : 'entsperrt';
        return back()->with('success','Gegenstand '. $blocked_str);
    }

}
