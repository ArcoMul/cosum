<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Profile;

class EquipeUserWithProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$users= User::all();
        $users = User::with('profile')->get();
     	$newProfiles = [];

        foreach ($users as $user){
            if (!$user->profile) {
                $newProfiles[] = [
                    'user_id' => $user->id,
                ];
            }
        }

     	Profile::insert($newProfiles);
    }
}
