<?php

use Illuminate\Database\Seeder;
use Dirape\Token\Facades\Facade;
use App\Gegenstand;

class InitItemHashesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $gegenstaende= Gegenstand::all();
        foreach ($gegenstaende as $gegenstand){
            if ($gegenstand->hash==null){
                $gegenstand->hash= Token::Unique('gegenstands', 'hash', 12);
                $gegenstand->save();
            }
        }
    }
}
