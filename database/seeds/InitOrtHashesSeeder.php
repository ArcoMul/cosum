<?php

use Illuminate\Database\Seeder;
use Dirape\Token\Facades\Facade;
use App\Ort;

class InitOrtHashesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orts= Ort::all();
        foreach ($orts as $ort){
            if ($ort->hash==null){
                $ort->hash= Token::Unique('orts', 'hash', 12);
                $ort->save();
            }
        }
    }
}
