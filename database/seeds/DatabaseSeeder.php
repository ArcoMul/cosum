<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(EquipeUserWithProfileSeeder::class);
        $this->call(InitItemHashesSeeder::class);
        $this->call(InitOrtHashesSeeder::class);
    }
}
