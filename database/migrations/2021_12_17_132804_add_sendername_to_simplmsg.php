<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSendernameToSimplmsg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('simple_msgs', function (Blueprint $table) {
            if (!Schema::hasColumn('simple_msgs','sender_name')) {
                $table->string('sender_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simple_msgs', function (Blueprint $table) {
            if (Schema::hasColumn('simple_msgs','sender_name')) {
                $table->dropColumn('sender_name');
            }
        });

    }
}
