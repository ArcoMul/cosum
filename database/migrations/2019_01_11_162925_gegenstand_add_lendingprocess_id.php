<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GegenstandAddLendingprocessId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gegenstands', function (Blueprint $table) {
            $table->integer('lendingprocess_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gegenstands', function (Blueprint $table) {
            $table->dropColumn('lendingprocess_id');
        });
    }
}
