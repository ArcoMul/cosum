<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revelations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('sender_id');
            $table->boolean('sender_news')->default(false);
            $table->integer('obj_id');
            $table->integer('rcpt_id');
            $table->boolean('recpt_news')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revelations');
    }
}
