<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpiryReminders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expiry_reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('expiry_date');
            $table->integer('sender_contact_id')->nullable();
            $table->integer('recepient_contact_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->string('status')->default('active'); //active, shot, done
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expiry_reminders');
    }
}
