<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Das Passwort muss mindestens sechs Zeichen und mit der Wiederholung übereinstimmen.',
    'reset' => 'Das Passwort wurde zurückgesetzt!',
    'sent' => 'Email verschickt! Sie enthält einen Link mit dem das Passwort zurückgesetzt werden kann.',
    'token' => 'Diese Anfrage ist ungültig.',
    'user' => "Kein Benutzer gefunden, der sich mit dieser Email-Adresse angemeldet hat.",

];
