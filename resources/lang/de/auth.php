<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Diese Eingabe passt nicht zu unseren Daten.',
    'throttle' => 'Zuviele Anmelde-Versuche. Bitte später wieder probieren in :seconds Sekunden.',

];
