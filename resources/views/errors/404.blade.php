@extends('layouts.errorpage')
@section('code', 404)

@section('title', __('Page Not Found'))

@section('image')
<div style="background-image: url('/svg/404.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __('Sorry, die angeforderte Seite konnte leider nicht gefunden werden.'))
