@extends ('layouts.app')

@section ('content')
    <h1>Ort anlegen</h1>
    {!! Form::open(['action' => ['OrteController@store', $required_by ], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        @if ($required_by)
            <div class="form-group">
                {{Form::hidden('required_by', $required_by, ['class' => 'form-control'])}}
            </div>
        @endif

        <div class="form-group">
            {{Form::label('address', 'Adresse')}}
            {{Form::text('address', '' , ['class' => 'form-control', 'placeholder' => 'Adresse'])}}
        </div>

        <div class="form-group">
            {{Form::label('beschreibung', 'Beschreibung')}} <i>&nbsp; Bitte Kontaktmöglichkeit angeben. </i>
            {{Form::textarea('beschreibung', '' , ['id' => 'article-ckeditor', 'class' => 'form-control' ])}}
        </div>
        {{Form::submit('anlegen', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
