@extends('layouts.app')

<style>
    .colour-picker-field {
      display: flex;
      justify-content: center;
    }

    .colour-picker-field__item {
      border: 1px solid gray;
      border-radius: 6px;
    }

    .colour-picker-field__item--text {
      width: 175px;
      padding: 1rem;
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
    }

    .colour-picker-field__item--picker {
      width: 50px;
      height: auto;
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
    }
</style>

@section('content')
        {!! Form::open(['action' => ['OrteController@update', $ort->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <h1>Ort bearbeiten</h1>
    <div class="row" id="head_row">
        <a href="/storage/location_images/{{$ort->location_logo_image}}">
             <img style="height:50px;" src="/storage/location_images/{{$ort->location_logo_image}}">
        </a>
        <strong style="padding:5px;background:#fff;">Ort:</strong>
        <strong style="padding:5px;background:#fff;">{{$ort->name}}</strong>
    </div>
    <div class="row top-buffer" style="">
        <div class="col-md-6">
            <div class="form-group">
                {{Form::label('name', 'Name')}}
                {{Form::text('name', $ort->name, ['id' =>"name_input", 'class' => 'form-control', 'placeholder' => 'Name'])}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            {{Form::label('color', 'Primär-Farbe')}}
            <div class="colour-picker-field">
              <input class='form-control colour-picker-field__item colour-picker-field__item--text' value="{{$ort->color}}" name="color" type="text" id="branding_color_text" onchange="handleSetColours('branding_color_text','branding_color_picker');"/>
              <input class="form-control colour-picker-field__item--picker" name="colour-picker" type="color" id="branding_color_picker" oninput="handleSetColours('branding_color_picker','branding_color_text');"/>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
       <div class="col-md-12">
            <div class="form-group">
                {{Form::label('address', 'Adresse')}}
                {{Form::text('address', $ort->address, ['class' => 'form-control', 'placeholder' => 'Adresse'])}}
                        <span>
                        <a class="input" href="https://nominatim.openstreetmap.org/search?q={{  urlencode($ort->address) }}">Auf Karte anzeigen</a>
                        </span>

            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
           Bild hochladen : {{Form::file('location_logo_image')}}
            *Maximale Dateigröße: 1MB. 1 Bild pro Gegenstand.
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                {{Form::label('beschreibung', 'Beschreibung')}}
                {{Form::textarea('beschreibung', $ort->beschreibung, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Beschreibung Text'])}}
            </div>
        </div>
    </div>
    <div class="row">
            {{Form::hidden('_method','PUT')}}
            {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
    </div>
        {!! Form::close() !!}
<script>
      var mytext = document.getElementById('branding_color_text');
      var mypicker = document.getElementById('branding_color_picker');

      function handleSetColours(id1, id2 ) {
        var item1 = document.getElementById(id1);
        var item2 = document.getElementById(id2);
	var colour = item1.value;
        item2.setAttribute('value', colour);

        var head_row=document.getElementById('head_row');
        head_row.setAttribute('style' , 'padding:20px; min-height: 75px; background-color: ' + colour +';');
      }

      if (mytext.value) {
	handleSetColours('branding_color_text','branding_color_text');
      }
</script>
@endsection
