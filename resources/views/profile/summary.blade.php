@extends('layouts.app')

@section('content')
        <h2>Meine Profil in Zahlen</h2>
        <div class="row">
            <div class="col-sm-4">
                Gegenstände:<strong> {{$gegenstaende->count()}}</strong>
            </div>

            <div class="col-sm-4">
                Orte:<strong> {{$orte->count()}}</strong>
            </div>

            <div class="col-sm-4">
                Kontakte:<strong> {{$contacts->count()}}</strong>
            </div>

            <div class="col-sm-4">
                Kreise:<strong> {{$contactcircles->count()}}</strong>
            </div>

            <div class="col-sm-4">
                Gemeinschaften:<strong> {{$regkeys->count()}}</strong>
            </div>

            <div class="col-sm-4">
                Offenlegungen:<strong> {{$revelations->count()}}</strong>
            </div>
	</div>
@endsection
