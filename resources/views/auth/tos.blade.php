@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
        <div class="col-md-8">
                    <p>
                    Bitte diesen Text lesen.
                    </p>
        </div>

        <div class="col-md-8">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <strong>{{$latest->title}}</strong>
                    </div>
                    <div class="panel-body">
                        {!! $latest->body !!}
                        <a class="btn btn-success front-btn" href="/tos/agree/{{$latest->id}}">{{ __('zustimmen') }}</a>.
                    </div>
                </div>
        </div>
</div>
</div>
@endsection
