@extends ('layouts.app')

@section ('content')
    <a href="/gesuche" class="btn btn-primary">zurück</a>
    <h1>Gesuch hinzufügen</h1>
    {!! Form::open(['action' => 'ItemRequestsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Titel')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Bitte in der Beschreibung eine Kontaktmöglichkeit angeben.')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>
        <p>
            Mit dem Klick auf "abschicken" erhalten alle CosumerInnen eine E-Mail, die in ihrem Profil die Zusendung von Cosum-Mails aktiviert haben.
        </p>
        {{Form::submit('abschicken', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
