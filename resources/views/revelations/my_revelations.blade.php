@extends('layouts.app')

@section('content')
    <h1>Meine Offenlegungen</h1>
    <h2>an Gemeinschaften</h2>
    @foreach ($communities as $community)
        <h3 class="btn-default btn-warning">{{$community->keyname}}</h3>
        <div class="row">
        @foreach ($community->community_items()->get()->where('user_id',auth()->user()->id) as $gegenstand)
                    <div class="well col-md-3 col-sm-3">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
                        <h3><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h3>
                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                        @if ($gegenstand->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">u
                                    @if ($gegenstand->isOverDue())
                                        fällig
                                    @else
                                        verliehen
                                    @endif
                            </span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date" class="{{$gegenstand->isOverDue()?'text-danger':''}}">{{$gegenstand->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
			    <a class="tag label label-danger" href="/community/{{$community->id}}/retract/{{$gegenstand->id}}"><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a>
                        @endif
                    </div>
        @endforeach

        </div>
    @endforeach
    <hr />
    <h1>Meine Offenlegungen</h1>
    <h2>an einzelne Personen (verbundene Kontakte)</h2>
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
    @if (request()->has("time"))
      <label class="btn btn-sm btn-default btn-secondary" onclick="sort('alpha');" name="lbl_radio_alpha">
        <input type="radio" name="options" id="option1" > Alphabetisch</input>
      </label>
      <label class="btn btn-sm btn-default btn-secondary active" onclick="sort('time');" name="lbl_radio_time">
        <input type="radio" name="options" id="option2"> Zeitlich</input>
      </label>
    @else
       <label class="btn btn-sm btn-default btn-secondary active" onclick="sort('alpha');" name="lbl_radio_alpha">
        <input type="radio" name="options" id="option1" > Alphabetisch</input>
      </label>
      <label class="btn btn-sm btn-default btn-secondary" onclick="sort('time');" name="lbl_radio_time">
        <input type="radio" name="options" id="option2"> Zeitlich</input>
      </label>
    @endif
    </div>
    @if(count($revelations) > 0)
        @foreach($revelations as $revelation)
            <div class="well">
                <div class="row">
                    <div class="col-md-3 col-sm-3 ">
                        <strong><a href="/gegenstaende/{{$revelation->obj_id}}">{{$revelation->obj->name}}</a></strong><br/>
                            @if ($revelation->obj->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$revelation->obj->cover_image}}">
                            @if ($revelation->obj->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                <b><label>{{$revelation->obj->category}}</label></b>
                            </span>
                            @endif
                    </div>
		    <div class="col-md-2 col-sm-2">
                        <strong>Von:<a href="/profil">mir</a></strong><br/>
                        <small>Geschrieben am {{$revelation->created_at}}</small>
                    </div>
		    <div class="col-md-2 col-sm-2">
                        <strong>An: <a href="/kontakte/{{$revelation->contact->id}}/edit">{{$revelation->contact->called}}</a></strong>
                    </div>
		    <div class="col-md-3 col-sm-3">
                        @if ($revelation->obj->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">verliehen</span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date">{{$revelation->obj->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">verfügbar</span>
                        @endif
                    </div>
		    <div class="col-md-2 col-sm-2">
                        {!!Form::open(['action' => ['RevelationController@destroy', $revelation], 'method' => 'POST', 'class' => 'pull-right'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('zurücknehmen', ['class' => 'btn btn-sm btn-danger'])}}
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p>Keine Offenlegungen gefunden.</p>
    @endif
@endsection
@section ('page_script_code')

        function sort(kind){
            var url = window.location.origin;
            console.log("wl:" +  window.location );

            if (kind == "time")
                window.location= url + "/meine_offenlegungen?time";
            if (kind == "alpha")
                window.location= url + "/meine_offenlegungen?alpha";
        }
@endsection
