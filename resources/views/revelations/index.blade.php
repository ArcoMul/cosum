@extends('layouts.app')

@section('content')
    <h1>Offenlegungen</h1>
    <h2>aus Gemeinschaften</h2>
    @foreach ($communities as $community)
        <h3 class="btn-default btn-warning">{{$community->keyname}}</h3>
        <div class="row">
        @foreach ($community->community_items()->get() as $gegenstand)
                    <div class="well col-md-3 col-sm-3">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                            @if ($gegenstand->blocked)
                                <img style="width:100%" src="/storage/cover_images/noimage.jpg">
                            @else
                                <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                            @endif
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
                        <h3><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h3>
                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                        @if ($gegenstand->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">
                                    @if ($gegenstand->isOverDue())
                                       fällig
                                    @else
                                        verliehen
                                    @endif
                            </span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date" class="{{$gegenstand->isOverDue()?'text-danger':''}}">{{$gegenstand->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
                        @endif
                    </div>
        @endforeach

        </div>
    @endforeach
    <hr />
    <h1>Offenlegungen</h1>
    <h2>von einzelnen Personen (verbundene Kontakte)</h2>
    @if(count($revelations) > 0)
        @foreach($revelations as $revelation)
            <div class="well">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <strong><a href="/gegenstaende/{{$revelation->obj_id}}">{{$revelation->obj->name}}</a></strong><br/>
                            @if ($revelation->obj->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
                        <img style="width:100%" src="/storage/cover_images/{{$revelation->obj->cover_image}}">
                           @if ($revelation->obj->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$revelation->obj->category}}</label></b>
                            </span>
                            @endif
                    </div>
                    <div class="col-md-2 col-sm-2">
                        @if ($revelation->sender)
                            <strong>Von: <a href="/kontakte/{{$revelation->sender->id}}/edit">{{$revelation->subj_sender_name}}</a></strong>
                        @else
                            <strong>Von: ? </strong>
                            <br />
                        @endif
                        <small>Geschrieben am {{$revelation->created_at}}</small>
                    </div>
		    <div class="col-md-2 col-sm-2">
                        <strong>AN:<a href="/profil">mich</a></strong><br/>
                    </div>
		    <div class="col-md-2 col-sm-2">
                        @if ($revelation->obj->lent)
                            <span class="verleih_status btn-sm btn-default btn-danger active">verliehen</span>
                            <div class='input-group date top-buffer' >
                                <span class="input-group-addon">
                                    <label for="lent_to_date">Bis:</label>
                                </span>
                                <span class="input-group-addon">
                                    <label for="lent_to_date">{{$revelation->obj->lent_to_date}}</label>
                                </span>
                            </div>
                        @else
			    <span class="verleih_status btn-sm btn-default btn-success active">verfügbar</span>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p>Keine Offenlegungen gefunden.</p>
    @endif
@endsection
