            @php
                $ort = null;
                foreach ($filters as $filter)
                    if ($filter && $filter['name'] == "Ort"){
                        $ort=$filter['ort'];
                    }
            @endphp
            <div class="ort_header" style="width:100%;height:100px; @if ($brandingColor) background-color:{{$brandingColor}}; @endif margin-bottom:10px;margin-top:-33px;">
                <div class="container" >
                    <div class="row"style="margin-top:33px;" >
                        <!-- logo -->
                        <div class="col-sm-2" style="@if (!($ort && $ort->location_logo_image)) border: 1px dashed gray; @endif min-height:50px;">
                        @if ($ort && $ort->location_logo_image)
                            <a href="/storage/location_images/{{$ort->location_logo_image}}">
                                <img style="height:50px" src="/storage/location_images/{{$ort->location_logo_image}}">
                            </a>
                        @endif
                        </div>
                        <!-- Name -->
                        <div class="col-sm-6">
                            <strong style="padding:5px;background:#fff;">Ort:</strong>
                            <strong style="padding:5px;background:#fff;">{{$brandingName}}</strong>
                        </div>
                        <!-- Kurz-Text -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                </div>
            </div>
