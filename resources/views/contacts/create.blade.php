@extends('layouts.app')

@section('content')
	<h1>Herstellen:</h1>
    {!! Form::open(['action' => ['ContactsController@store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        @if ($corr_token)
        <h2><a href="/kontakte?corr_token={{$corr_token}}">Kontakt auswählen</a> </h2>
        @endif
        <div class="form-group">
            {{Form::label('called', 'Nenne ich:')}}
            {{Form::text('called', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('contact_token', 'Kontaktmarke eines Anderen, falls bekannt:')}}
            {{Form::text('contact_token', $corr_token, ['class' => 'form-control', 'placeholder' => 'Kontaktmarke'])}}
        </div>
    {{Form::submit('speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
