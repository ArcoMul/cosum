@extends('layouts.app')

@section('content')
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Liste der Kontakte</h3>
Lege einen Kontakt an, um an eine Person Gegenstände zu verleihen. <br/>
Du kannst auch einen Kontakt mit einem angemeldeten Cosumer verbinden. <br/>
Damit kannst du deine Gegenstände gezielt über "Gegenstand offenlegen" sichtbar machen. </br>

            <a href="/kontakte/create" class="btn btn-primary top-buffer">Kontakt anlegen</a>
        </div>
        <div class="panel-body">
     @if (count($contacts) > 0)
        @foreach ($contacts as $contact)
                <div class="col-md-6 col-sm-6">
                    @if ($corr_token)
                        <h4>
                            <a href="/kontakte/{{$contact->id}}/edit?corr_token={{$corr_token}}">{{$contact->called}}</a>
                        </h4>
                    @elseif ($obj_id && !$lend)
                        <h4>
                        @if ($contact->status == 'made')
                            <a href="/offenlegungen/create?obj_id={{$obj_id}}&rcpt_id={{$contact->id}}">{{$contact->called}} </a>
                        @else
                            <label>{{$contact->called}} </label>
                        @endif
                        </h4>
                    @elseif ($lend)
                        <h4>
                            <a href="/gegenstaende/{{$lend}}/edit?lent_to={{$contact->id}}">{{$contact->called}}</a>
                        </h4>

                    @else
                        <h4>
                            <a href="/kontakte/{{$contact->id}}/edit">{{$contact->called}}</a>
                        </h4>
                    @endif
                        @if ($contact->status == 'made')
                                <span class="glyphicon glyphicon-link">
                                    <small> <strong>@lang('contact.status.'.$contact->status)</strong> </small>
                                </span>
                        @endif
                        @if ($contact->deleted_user)
                            (gelöschter Benutzer)
                        @endif
                        <br />
                        <small>Angelegt am {{$contact->created_at}}</small>
                </div>
        @endforeach
    @else
        <p>Keine Bekanntschaften</p>
    @endif
        </div>
    </div>
</div>
<div class="col-md-6">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Kontaktkreise und Gemeinschaften</h3>
                    Gruppiere deine Kontakte wie du es willst! <br />
                    <a href="kontaktkreise/create" class="btn btn-primary top-buffer" style="margin-right:10px;">Kontaktkreis anlegen</a>
                    <a href="#wasistdas" data-toggle="collapse" class="header_link">
                            Was ist das?
                    </a>
                    <p class="top-buffer collapse" id="wasistdas">
                       Mit <strong> Kontaktkreis </strong> ist ein Personenkreis gemeint, den du selber definierst und individuell gestaltest. Das hilft dir deine Kontakte zu organsieren, wie in einem Adressbuch. <br />
                       <strong>Gemeinschaften</strong> sind Zusammenschlüsse von Personen. Wenn du Teil einer Gemeinschaft bist, dann haben wir für dich einen besonderen Kreis angelegt, in dem die Gemeinschaftsimitglieder eingetragen sind. Dadurch kannst du Gegenstände mit einem Schlag einer größeren Zahl von Personen offenlegen.
                    </p>

                </div>

                <div class="panel-body">
                    @if (count($contactcircles) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach ($contactcircles as $contactcircle)
                                <tr>
                                    <td>
                                        <a class="tag label label-primary">{{$contactcircle->name}}</a>

                                    @if ($contactcircle->isCommunityCircle())
                                        <label class="tag label label-warning">Gemeinschaft</label>
                                    @endif
                                    </td>
                                    <td><a href="kontaktkreise/{{$contactcircle->id}}/edit" class="btn btn-default">bearbeiten</a></td>
                                    <td>
                                        {!!Form::open(['action' => ['ContactCirclesController@destroy', $contactcircle->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:40px;">
                                    @foreach ($contactcircle->contacts as $contact )
                                        @if ($contact->deleted_user)
                                            <a class="tag label label-danger" href="/kontakte/{{$contact->id}}/edit">{{$contact->called}}
                                        @else
                                            <a class="tag label label-success" href="/kontakte/{{$contact->id}}/edit">{{$contact->called}}
                                        @endif
                                        <i class="remove glyphicon glyphicon-user glyphicon-white"></i>
                                    </a>
                                    @endforeach
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>Keine Kontaktkreis</p>
                    @endif
                </div>
            </div>
</div>
@endsection
