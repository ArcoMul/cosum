@extends('layouts.app')

@section('content')
    <a href="{{ URL::previous() }}" class="btn btn-default">Zurück</a>
    <h1>{{$gegenstand->name}}</h1>
    <h2>{{$ort->name}}</h2>

	 <div class="row">
		<div class="col-md-6 col-sm-6">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif

                            @if (!$gegenstand->blocked)
                                <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                            @else
                                    @if (Auth::guest())
                                        <img style="width:100%" src="/storage/cover_images/noimage.jpg">
                                    @else
                                        @if (Auth::user()->isOperator() || Auth::user()->id == $gegenstand->user_id)
                                            <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                                            <span class="blockedbanner btn-sm btn-default btn-danger active input-group-addon" >Oh! Etwas stimmt mit dem Bild nicht.</span>
                                        @else
                                            <img style="width:100%" src="/storage/cover_images/noimage.jpg">
                                        @endif
                                    @endif

                            @endif



                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
		</div>
		<div class="col-md-6 col-sm-6">

			<br><br>
			<div>
			{!!$gegenstand->beschreibung!!}
			</div>
			<hr>
			<div>
			Ort: <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong>
                        @if ($ort->address)
                            <div class='input-group top-buffer' >
                                        <span class="">
                                        <span class="glyphicon glyphicon-pushpin"></span>
                                        </span>
                                        <span>
                                        <a class="input" href="https://nominatim.openstreetmap.org/search?q={{  urlencode($ort->address) }}">Auf Karte anzeigen</a>
                                        </span>
                            </div>
                        @endif

			</div>
			<small>Angelegt am {{$gegenstand->created_at}} </small>
			<div>
			    @if ($gegenstand->lent)
                                <span class="verleih_status btn-sm btn-default btn-danger active">verliehen</span>
                                @if ($gegenstand->isOverDue())
                                    <strong class="text-danger">fällig</strong>
                                @endif

                                <div class='input-group date top-buffer' >
                                    <span class="input-group-addon">
                                        <label for="lent_to_date">Bis:</label>
                                    </span>
                                    <span class="input-group-addon">
                                        <label for="lent_to_date" class="{{$gegenstand->isOverDue()?'text-danger':''}}">{{$gegenstand->lent_to_date}}</label>
                                    </span>
                                </div>

			    @else
				<span class="verleih_status btn-xs btn-default btn-success active">verfügbar</span>
			    @endif
                            <br />
			    <a href="/kontaktieren/{{$gegenstand->id}}" class="btn btn-sm btn-success front-btn">Leihanfrage</a>
			</div>
			<hr>
                        @if (!Auth::guest() && Auth::user()->isOperator())
                            <a href="/gegenstaende/{{$gegenstand->id}}/tglblock" class="btn btn-danger">{{($gegenstand->blocked)? 'entsperren' : 'sperren'}}</a>
                        @endif

			@if (!Auth::guest())
				@if(Auth::user()->id == $gegenstand->user_id)
				    <a href="/gegenstaende/{{$gegenstand->id}}/edit" class="btn btn-default">bearbeiten</a>
				    {!!Form::open(['action' => ['GegenstaendeController@destroy', $gegenstand->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
					{{Form::hidden('_method', 'DELETE')}}
					{{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
				    {!!Form::close()!!}
				@endif
			@endif
		</div>
	</div>
@endsection
