@extends('layouts.app')

@section('content')
    <h1>Gegenstand anlegen</h1>
    {!! Form::open(['action' => 'GegenstaendeController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
	</div>
        <div class="form-group">
            <label for="ortname">Leihort:</label>
            <?php // select code manually because of Form::select bug  ?>
            <select id="ortname" onchange="synch_id(this);">
                    <option value="0"> Bitte Ort auswählen  </option>
                    <?php
                            foreach($ortnamen as $key => $name) {
                    ?>
                                    <option value= <?php echo "\"$key\">". $name ; ?></option>
                    <?php
                            }
                    ?>
            </select>
	</div>
        <div class="form-group" hidden="hidden">
            {{Form::label('sel_ort', 'Select OrtId')}}
	    {{Form::select('sel_ortid', $ortids , '', ['id' => 'sel_ortid']) }}
	</div>
	<div class="form-group" hidden="hidden">
            {{Form::label('ortid', 'OrtId')}}
	    {{Form::text('ortid', '', ['id' => 'ortid']) }}
        </div>

        <div class="form-group">
            {{Form::label('beschreibung', 'Beschreibung')}}
            {{Form::textarea('beschreibung', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Beschreibung Text'])}}
        </div>
        <div class="form-group">
            {{Form::file('cover_image')}}
            *Maximale Dateigröße: 2MB.
        </div>
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection

@section('page_script_code')
	(function() {
	})();
	function synch_id(that){
		var sel= document.getElementById('sel_ortid');
		sel.value=that.value;
		document.getElementById('ortid').value=sel.options[sel.selectedIndex].text;
	}
@endsection
