<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/body_pattern.css') }}" rel="stylesheet" title="">
    <link href="{{ asset('css/blue_board.css') }}" rel="stylesheet" title="blueboard">
    <link href="{{ asset('css/post_styles.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include ('inc.opnavbar')
        <div class="container">
            @include ('inc.messages')
            @yield ('content')
        </div>
	@include ('inc.footer')
    </div>

    <script>
            @yield ('page_script_code')
    </script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
    var element =  document.getElementById('article-ckeditor');
    if (typeof(element) != 'undefined' && element != null)
    {
        CKEDITOR.replace( 'article-ckeditor', {
            removePlugins: 'sourcearea'
        });
        var second_editor= document.getElementById('article-ckeditor2');
        if (second_editor){
            CKEDITOR.replace( 'article-ckeditor2', {
                removePlugins: 'sourcearea'
            });
        }else{
            console.log('No second editor found.');
        }

    }
    </script>
</body>
</html>
