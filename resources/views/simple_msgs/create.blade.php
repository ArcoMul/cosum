@extends('layouts.app')

@section('content')
    <h1>Anfrage stellen</h1>
    {!! Form::open(['action' => 'SimpleMsgController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <h3>{{$gegenstand->name}}</h3>
    <div class="row">
        <div class="form-group top-buffer" hidden="hidden">
            {{Form::number('item_id', $gegenstand->id)}}
	    {{Form::number('from_user_id', auth()->user()->id ) }}
	    {{Form::number('to_user_id', $gegenstand->user_id ) }}
	</div>
	<div class="col-md-4 col-sm-4">
                            @if ($gegenstand->gift)
                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                            @endif
			<img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                           @if ($gegenstand->category)
                            <span class="category_lbl btn-sm btn-default btn-info">
                                    <b><label>{{$gegenstand->category}}</label></b>
                            </span>
                            @endif
        </div>

        <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4 top-buffer">
            <label for="sender_name">Mein Absendername</label><br />
            <input id="name" type="text" class="form-control" name="sender_name" value="{{ $sender_name }}">
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 top-buffer">
            <label for="daterange">Wunsch-Zeitraum</label><br />
            <div class="input-group input-daterange" id="daterange">
                <div class="input-group-addon">von</div>
                <input type="text" class="form-control picker" data-provide="datepicker" value="" name="desired_date_from">
            </div>
            <div class="input-group input-daterange" id="daterange">
                <input type="text" class="form-control picker" data-provide="datepicker" value="" name="desired_date_to">
                <div class="input-group-addon">bis</div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer">
            {{Form::label('text', 'Nachricht')}}
            {{Form::textarea('text', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Nachrichten Text'])}}
        </div>
        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 top-buffer" hidden="hidden">
            <label for="signal_go">Ich sag' mal ja.</label>
            <input type="radio" name="signal" value="0" id="signal_go">
            <br />
            <label for="signal_no"> Geht leider nicht.</label>
            <input type="radio" name="signal" value="1" id="signal_no">

            <?php //, 'readonly' => 'readonly' ?>
            {{Form::textarea('reply', '', ['id' => 'article-ckeditor2-dis', 'class' => 'form-control',  'placeholder' => 'Antwort Text'])}}
        </div>

        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
        </div>

    </div>
    {!! Form::close() !!}
    @endsection

    @section ('page_scripts')
  <script src="/js/jquery-1.12.4.js"></script>
  <script src="/js/moment.min.js"></script>
  <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript">
        $('.input-daterange input').each(function() {
            $(this).datepicker();
        });
        (function() {
            //manually create language, for buggy include
            $.fn.datepicker.defaults.language = "de";
            $.fn.datepicker.noConflict;
            $.fn.datepicker.dates['de'] = {
                days: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                daysShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                daysMin: ["So", "Mo", "di", "Mi", "do", "Fr", "Sa"],
                months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Julie", "August", "September", "Oktober", "November", "Dezember"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
                today: "Heute",
                clear: "Clear",
                format: "dd.mm.yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 0
            };

	    $('.picker').datepicker({language:'de'});
	})();
    </script>

    @endsection
