@extends ('layouts.app')
@section ('content')
<h1>{{$community->keyname}}</h1>
@if (auth()->user()->isOperator())
    <a class="btn btn-sm btn-danger" href="/regkeys/{{$community->id}}/edit">bearbeiten</a>
@endif

<hr />
    <div class="row">
    <div class="col-md-12 col-sm-12">
        {!!$community->community_description !!}
    </div>
    </div>
@endsection
