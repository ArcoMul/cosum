@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <a href="/gegenstaende/create" class="btn btn-primary">Gegenstand anlegen</a>
                   <h3>Meine Gegenstände</h3>
                    Anzahl: {{count($gegenstaende)}}
                    @if(count($gegenstaende) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($gegenstaende as $gegenstand)
                                <tr>
                                    <td colspan="3">{{$gegenstand->name}}</td>
                                </tr>
                                <tr>
                                    <td>
                                        @if ($gegenstand->lent)
			                    <span class="verleih_status btn-xs btn-default btn-danger active">verliehen</span>
                                        @else
                                            <span class="verleih_status btn-xs btn-default btn-success active">verfügbar</span>
                                        @endif
                                    </td>
                                    <td><a href="/gegenstaende/{{$gegenstand->id}}/edit" class="btn btn-xs btn-default">Edit</a></td>
                                    <td>
                                        {!!Form::open(['action' => ['GegenstaendeController@destroy', $gegenstand->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('Delete', ['class' => 'btn btn-xs btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>Du hast keine Gegenstände angelegt.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
