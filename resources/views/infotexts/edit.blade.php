@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/infotexts" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>Infotext bearbeiten</h1>
    {!! Form::open(['action' => ['InfotextsController@update', $infotext->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Titel')}}
            {{Form::text('title', $infotext->title, ['class' => 'form-control', 'placeholder' => 'Titel'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Text')}}
            {{Form::textarea('body', $infotext->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Speichern', ['class'=>'btn btn-sm btn-info front-btn'])}}
    {!! Form::close() !!}
@endsection
