@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/infotexts" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>{{$infotext->title}}</h1>
    <br><br>
    <div class="well">
        {!!$infotext->body!!}
    </div>
    <hr>
    <small>Verfasst am {{$infotext->created_at}} von {{$infotext->user->name}}</small>
    <hr>
    <a href="/infotexts/{{$infotext->id}}/edit" class="btn btn-sm btn-info front-btn">bearbeiten</a>
@endsection
